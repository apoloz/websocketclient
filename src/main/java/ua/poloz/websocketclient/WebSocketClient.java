package ua.poloz.websocketclient;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;


/**
 *
 * @author poloz
 */
public class WebSocketClient {

    private URI mUrl;
    private Socket mSocket;
    private InputStream mInput;
    private OutputStream mOutput;

    /**
     * Creates a new WebSocket targeting the specified URL.
     *
     * @param url The URL for the socket.
     */
    public WebSocketClient(URI url) {
        mUrl = url;
    }


    /**
     * Returns the underlying socket;
     */
    public Socket getSocket() {
        return mSocket;
    }

    /**
     * Establishes the connection.
     *
     * @throws java.io.IOException
     */
    public void connect() throws java.io.IOException {
        String host = mUrl.getHost();
        String path = mUrl.getPath();

        String query = mUrl.getQuery();
        if (query != null) {
            path = path + "?" + query;
        }

        String origin = "http://" + host;

        mSocket = createSocket();
        int port = mSocket.getPort();
        host = host + ":" + port;

        mOutput = mSocket.getOutputStream();
        StringBuilder extraHeaders = new StringBuilder();

        String request = "GET " + path + " HTTP/1.1\r\n"
                + "Upgrade: WebSocket\r\n"
                + "Connection: Upgrade\r\n"
                + "Host: " + host + "\r\n"
                + "Origin: " + origin + "\r\n"
                + extraHeaders.toString()
                + "\r\n";
        mOutput.write(request.getBytes());
        mOutput.flush();

        mInput = mSocket.getInputStream();
    }

    private Socket createSocket() throws java.io.IOException {
        String host = mUrl.getHost();
        int port = mUrl.getPort();
            return new Socket(host, port);
    }

    /**
     * Sends the specified string as a data frame.
     *
     * @param str The string to send.
     * @throws java.io.IOException
     */
    public void send(String str) throws java.io.IOException {

        mOutput.write(0x00);
        mOutput.write(str.getBytes("UTF-8"));
        mOutput.write(0xff);
        mOutput.flush();
    }

    /**
     * Receives the next data frame.
     *
     * @return The received data.
     * @throws java.io.IOException
     */
    public String recv() throws java.io.IOException {

        StringBuilder buf = new StringBuilder();
        int b = 0;
        while (true) {
            b = mInput.read();
            if (b == 0xff || b == -1) {
                break;
            }
            buf.append((char) b);
        }
        return new String(buf.toString().getBytes(), "UTF8");
    }

    /**
     * Closes the socket.
     *
     * @throws java.io.IOException
     */
    public void close() throws java.io.IOException {
        mInput.close();
        mOutput.close();
        mSocket.close();
    }

    /**
     * Quick echo test code.
     *
     * @param args
     */
    public static void main(String[] args) {
        try {
            WebSocketClient ws = new WebSocketClient(new URI("ws://[0:0:0:0:0:0:0:0]:9000"));
            ws.connect();

            String request = "Hello";
            ws.send(request);
            String response = ws.recv();
            System.out.println(request);
            System.out.print(response);
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }

}
